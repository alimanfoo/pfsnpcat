#!/usr/bin/env python

# standard libraries
import time
import sys

# 3rd party libraries
from htsql import HTSQL

# initialise db connection
db = HTSQL('sqlite:db.sqlite3')

# define queries to benchmark
if len(sys.argv) > 1:
    queries = sys.argv[1:]
else:
    queries = [
#        "/snp",
        "/count(snp)",
        "/snp.limit(50)",
        "/snp.filter(chromosome='MAL1')",
        "/count(snp.filter(chromosome='MAL1'))",
        "/snp.filter(chromosome='MAL1').limit(50)",
        "/snp.filter(chromosome='MAL1').limit(50, 100)",
        "/snp.filter(chromosome='MAL1').limit(50, 1000)",
        "/snp.filter(chromosome='MAL1'&position>100000&position<200000)",
        "/count(snp.filter(chromosome='MAL1'&position>100000&position<200000))",
        "/snp.filter(chromosome='MAL1'&position>100000&position<200000).limit(50)",
        "/snp.filter(geneid='MAL7P1.27')",
        "/snp.filter(genedescription~'transport')",
        "/snp.filter(genetext~'transport')",
        "/snp.filter(genetext~'transport').limit(50)",
        "/snp.filter(genetext~'transport').limit(50, 2000)",
        "/count(snp.filter(genetext~'transport'))",
        "/snp.filter(genetext~'pfcrt')",
        "/snp.filter(genetext~'pfcrt').limit(50)",
#        "/snp.sort(chromosome+,position+)",
        "/snp.sort(chromosome+,position+).limit(50)",
        "/snp.sort(chromosome+,position+).limit(50, 100)",
        "/snp.sort(chromosome+,position+).limit(50, 100000)",
        "/snp.sort(daf_afr+).limit(50)",
        "/snp.sort(daf_afr-).limit(50)",
        "/snp.filter(daf_afr<0.2&daf_sea>0.8)",
        "/count(snp.filter(daf_afr<0.2&daf_sea>0.8))",
        "/snp.filter(daf_afr<0.2&daf_sea>0.8).sort(daf_sea+)",
        "/snp.filter(daf_afr<0.2&daf_sea>0.8).sort(daf_sea-)",
        "/snp.filter(daf_afr<0.2&daf_sea>0.8).sort(daf_sea-).limit(50)",
        "/snp.filter(daf_afr<0.2&daf_sea>0.8).sort(chromosome,position)"
    ]

# run queries
for query in queries:
    print query
    before = time.clock()
    count = sum(1 for _ in db.produce(query))
    after = time.clock()
    print '%s rows in %ss' % (count, after - before)
    
