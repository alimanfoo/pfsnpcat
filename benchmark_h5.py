#!/usr/bin/env python

import time
import tables
from petl import data
from petlx.hdf5 import fromhdf5

queries = [
    """(daf_afr < 0.1) & (daf_sea > 0.9)""",
    """(chromosome == 'MAL1') & (position > 100000) & (position < 200000)"""
]

h5file = tables.openFile('db.h5')

#before = time.clock()
#count = sum(1 for _ in fromhdf5(h5file, '/pfpg/snp'))
#after = time.clock()
#print '%s rows in %ss' % (count, after - before)

for query in queries:
    print query
    for _ in range(5): # repeat each query 5 times
        time.sleep(1) # sleep for 1 second between queries
        before = time.clock()
        count = sum(1 for _ in data(fromhdf5(h5file, '/pfpg/snp', condition=query)))
        after = time.clock()
        print '%s rows in %ss' % (count, after - before)

h5file.close()
