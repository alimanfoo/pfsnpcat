from htsql import HTSQL

# The address of the database in the form:
#   engine://user:pass@host:port/database
DB = 'sqlite:/home/aliman/src/bitbucket/pfsnpcat/db.sqlite3'

application = HTSQL(DB, {
	    'tweak.autolimit': {'limit': 10000},
	    'tweak.timeout': {'timeout': 10
	    'tweak.cors': {'origin': '*'},
	    'tweak.meta': {},
	     
	    })