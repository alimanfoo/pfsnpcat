#!/usr/bin/env python

#
# This script prepares the SNP data and loads it into
# an sqlite database.
# 

# dependencies from standard python libraries
import os.path
import time
import tables # apt-get install python-tables

# 3rd party dependencies
from petl import * # pip install petl
from petlx.hdf5 import tohdf5 # pip install petlx

# main routine
if __name__ == '__main__':

    print 'prepare the data for loading...'
    FILE = 'snp_complete_pgv.csv'
    t0 = fromcsv(FILE)
    t1 = convertall(t0, lambda v: -1 if v == '-' else v) # normalise nulls
    type_conversions = {
        'ID': int, 
        'Position': int, 
        "NRAF_AFR": float,
        "NRAF_SEA": float,
        "NRAF_PNG": float,
        "LCAF_AFR": float,
        "LCAF_SEA": float,
        "LCAF_PNG": float,
        "MAF_AFR": float,
        "MAF_SEA": float,
        "MAF_PNG": float,
        "DAF_AFR": float,
        "DAF_SEA": float,
        "DAF_PNG": float
    }
    t2 = fieldconvert(t1, type_conversions) # prepare datatypes

    # add a column with all gene text for gene search
    def make_genetext(row):
        text = ''
        for f in ['GeneID', 'GeneAliases', 'GeneDescription']:
            if row[f] is not None:
                text += ' ' + str(row[f]).lower()
        return text
    t3 = addfield(t2, 'GeneText', make_genetext)

    t4 = setheader(t3, [f.lower() for f in header(t3)])

    print 'load the database...'
    h5file = tables.openFile('db.h5', 'w')
    p = progress(t4, 20000)
    tohdf5(p, h5file, '/pfpg', 'snp', create=True, createparents=True, expectedrows=421000)
    
    print 'index some fields...'
    snp = h5file.root.pfpg.snp
    snp.cols.chromosome.createIndex()
    snp.cols.position.createIndex()
    snp.cols.daf_afr.createIndex()
    snp.cols.daf_sea.createIndex()
    snp.cols.daf_png.createIndex()

    print 'all done, clean up'
    h5file.flush()
    h5file.close()

