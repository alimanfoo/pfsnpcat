from petl.interactive import *
from matplotlib.pyplot import *
t0 = fromcsv('results.csv')
t1 = t0.cut('allThreads', 'elapsed').convertnumbers()
f = facetcolumns(t1, 'allThreads')
labels = sorted(f.keys())
x = [f[l]['elapsed'] for l in labels]
boxplot(x)
xlabel('concurrency')
ylabel('request time (ms)')
show()
