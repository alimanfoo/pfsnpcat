from petl.interactive import *

t0 = fromsqlite3('/home/aliman/src/bitbucket/pfsnpcat/db.sqlite3', 'select chromosome, position, nraf_afr, nraf_sea, nraf_png from snp')

for chrn in range(1, 15):
    chr = 'MAL%s' % chrn
    for pop in ('afr', 'sea', 'png'):
        var = 'nraf_%s' % pop
        t = t0.selecteq('chromosome', chr).selectgt(var, 0.01)
        a = multirangeaggregate(t, keys=('position', var), widths=(100000, 0.1), aggregation=len, mins=(0, 0), maxs=(None, 1))
        o = fieldmap(a)
        o['entity'] = 'key', lambda v: str(v[1][1]) # nraf bin end as entity
        o['date'] = 'key', lambda v: v[0][1] / 1000 # position in kb as pseudo-year
        o['nraf'] = 'key', lambda v: v[1][1]
        o['snp_count'] = 'value' 
        fn = 'static/%s_%s.json' % (chr, pop)
        print 'dumping %s' % fn
        tojsonarrays(o, fn)
        

