var INIT_TEST = {};

// Sample test
INIT_TEST.init = function(addButton) {
	// Aliases
	var OPERATORS = expcat.cql.Operator;
	var propertyMap = INIT_TEST.getPropertyDictionary();
	var operatorMap = OPERATORS.getSymbolMap();

	var containers = INIT_TEST.generateContainers();
	var manager = new expcat.plugins.QueryComposerUIManager(operatorMap, propertyMap, containers, "query-delimiter");

	// Handler for the button.
	addButton.bind("click", function addCondition() {
		manager.addCondition();
	});

};

// Nesting containers.
INIT_TEST.generateContainers = function() {
	// Container panels.
	var nestedPanel = $("<div></div>", {
		"class" : "nestedPanel"
	});

	var nestedDelimiter = $("<div></div>", {
		"class" : "nestedDelimiter"
	});

	return [nestedPanel, nestedDelimiter];
}

// Sample dictionary
INIT_TEST.getPropertyDictionary = function() {
	var Property = expcat.cql.Property;
	var propertyMap = {};

	propertyMap["Chromosome"] = new Property({
		name : "Chromosome",
		type : "STRING",
		description : "The name of the sequence region in the P. falciparum (3D7) reference sequence in which the variation is found",
		minimum : null,
		maximum : null,
		allowedValues : ["MAL1", "MAL2", "MAL3", "MAL4", "MAL5", "MAL6", "MAL7", "MAL8", "MAL9", "MAL10", "MAL11", "MAL12", "MAL13", "MAL14"]
	});

	propertyMap["Position"] = new Property({
		name : "Position",
		type : "INTEGER",
		description : "The base coordinate at which the variation occurs.",
		minimum : 1,
		maximum : null,
		allowedValues : null
	});

	propertyMap["ReferenceAllele"] = new Property({
		name : "ReferenceAllele",
		type : "STRING",
		description : "The nucleotide that occurs in the P. falciparum (3D7) reference sequence at the given position.",
		allowedValues : ["A", "T", "G", "C"]
	});

	propertyMap["NonRefAllele"] = new Property({
		name : "NonRefAllele",
		type : "STRING",
		description : "The alternative nucleotide that is found in one or more samples.",
		allowedValues : ["A", "T", "G", "C"]
	});

	propertyMap["OutgroupAllele"] = new Property({
		name : "OutgroupAllele",
		type : "STRING",
		description : "The nucleotide found at a homologous position in P. rechenowi.",
		allowedValues : ["A", "T", "G", "C"]
	});

	propertyMap["AncestralAllele"] = new Property({
		name : "AncestralAllele",
		type : "STRING",
		description : "The allele believed to be ancestral.",
		allowedValues : ["A", "T", "G", "C"]
	});

	propertyMap["DerivedAllele"] = new Property({
		name : "DerivedAllele",
		type : "STRING",
		description : "The allele believed to be derived.",
		allowedValues : ["A", "T", "G", "C"]
	});

	propertyMap["GeneID"] = new Property({
		name : "GeneID",
		type : "STRING",
		description : "The identifier for the gene annotation found at that position.",
		allowedValues : null
	});

	return propertyMap;
};
